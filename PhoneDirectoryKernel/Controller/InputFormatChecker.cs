﻿using System.Text.RegularExpressions;

namespace PhoneDirectoryKernel.Controller
{
    public interface IInputFormatChecker
    {
        bool CheckName(string name);

        bool CheckPhoneNumber(string number);
    }

    public class InputFormatChecker : IInputFormatChecker
    {

        private readonly Regex namePattern = new Regex(@"^[a-zA-Zа-яА-Я]+$");

        private readonly Regex numberPattern = new Regex(@"^\d{3,12}$");

        public bool CheckName(string name)
        {
            return namePattern.IsMatch(name);
        }

        public bool CheckPhoneNumber(string number)
        {
            return numberPattern.IsMatch(number);
        }
    }
}
