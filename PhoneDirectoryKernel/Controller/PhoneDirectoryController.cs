﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Text.RegularExpressions;
using PhoneDirectoryKernel.Model;
using PhoneDirectoryKernel.Properties;

namespace PhoneDirectoryKernel.Controller
{
    public class PhoneDirectoryController 
    {
        private PhoneDirectoryModel privateDirectoryStorage;

        private InputFormatChecker checker;

        public bool AddContactToDirectory(string name, string surname, string number, out string message, out ContactInformation contactInformation)
        {
            contactInformation = null;
            if (!checker.CheckName(name))
            {
                message = CommonResources.IncorrectName;
                return false;
            }
            if (!checker.CheckName(surname))
            {
                message = CommonResources.IncorrectSurname;
                return false;
            }
            if (!checker.CheckPhoneNumber(number))
            {
                message = CommonResources.IncorrectNumber;
                return false;
            }
            var contact = new ContactInformation(name, surname, number);
            bool contactAdded = privateDirectoryStorage.AddContact(contact);
            if (contactAdded)
            {
                message = "Contact added";
                contactInformation = contact;
                return true;
            }
            message = "Contact is already exist";
            return false;
        }

        public bool RemoveContact(string name, string surname, out string message, out  ContactInformation contactInformation)
        {
            contactInformation = null;
            if (!checker.CheckName(name))
            {
                message = CommonResources.IncorrectName;
                return false;
            }
            if (!checker.CheckName(surname))
            {
                message = CommonResources.IncorrectSurname;
                return false;
            }
            if (privateDirectoryStorage.RemoveSpecifiedContact(name, surname, out contactInformation))
            {
                message = "Contact removed";
                return true;
            }
            message = "Contact not found";
            return false;
        }

        public bool GetContact(string name, string surname, out string message, out ContactInformation contactInformation)
        {
            contactInformation = null;
            if (!checker.CheckName(name))
            {
                message = CommonResources.IncorrectName;
                return false;
            }
            if (!checker.CheckName(surname))
            {
                message = CommonResources.IncorrectSurname;
                return false;
            }
            if (privateDirectoryStorage.GetContactsInformationByNameAndSurname(name,
                    surname, out ContactInformation foundContact))
            {
                contactInformation = foundContact;
                message = "Contact found";
                return true;
            }
            message = "Contact not found";
            return false;
        }

        public PhoneDirectoryController()
        {
            privateDirectoryStorage = new PhoneDirectoryModel();
            checker = new InputFormatChecker();
        }
    }
}
