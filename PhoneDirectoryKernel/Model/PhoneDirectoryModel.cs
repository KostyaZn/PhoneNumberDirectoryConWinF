﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PhoneDirectoryKernel.Model
{
    public class ContactInformation : IEquatable<ContactInformation>
    {
        public string Name { get; }
        public string Surname { get; }
        public string PhoneNumber { get; }

        public ContactInformation(string name, string surname, string phoneNumber)
        {
            Name = name;
            Surname = surname;
            PhoneNumber = phoneNumber;
        }

        public bool Equals(ContactInformation other)
        {
            if (ReferenceEquals(null, other)) return false;
            return Name == other.Name && Surname == other.Surname;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((ContactInformation) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (Name != null ? Name.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Surname != null ? Surname.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (PhoneNumber != null ? PhoneNumber.GetHashCode() : 0);
                return hashCode;
            }
        }
    }

    public class PhoneDirectoryModel
    {
        private Dictionary<string, List<ContactInformation>> nameToTotalInformationConnector = 
            new Dictionary<string, List<ContactInformation>>();
        private Dictionary<string, List<ContactInformation>> surnameToTotalInformationConnector = 
            new Dictionary<string, List<ContactInformation>>();
        public bool AddContact(ContactInformation contactInformation)
        {
            if (nameToTotalInformationConnector.TryGetValue(contactInformation.Name,
                out List<ContactInformation> contactsByNames))
            {
                var tryFindExistingContactInNamesCollection = contactsByNames.FindAll(x => x.Equals(contactInformation));
                if (tryFindExistingContactInNamesCollection.Count>0)
                {
                    if (tryFindExistingContactInNamesCollection.Count > 1)
                    {
                        throw new DirectoryStorageException("More than one contact have the same name and surname" +
                                                                  Environment.NewLine + "State is invalid");
                    }
                    return false;
                }
                contactsByNames.Add(contactInformation);
                surnameToTotalInformationConnector[contactInformation.Surname].Add(contactInformation);
                return true;
            }
            nameToTotalInformationConnector[contactInformation.Name]= new List<ContactInformation> {contactInformation};
            surnameToTotalInformationConnector[contactInformation.Surname]= new List<ContactInformation> { contactInformation };
            return true;
        }
        public bool RemoveSpecifiedContact(string name, string surname, out ContactInformation contactInformation)
        {
            contactInformation = null;
            if (nameToTotalInformationConnector.TryGetValue(name, 
                out List<ContactInformation> contactsWithSuchName))
            {
                if (surnameToTotalInformationConnector.TryGetValue(surname,
                    out List<ContactInformation> contactsWithSuchSurname))
                {
                    var contact = contactsWithSuchSurname.Intersect(contactsWithSuchName).ToList();
                    if (contact.Count > 1)
                    {
                        throw new DirectoryStorageException("More than one contact have the same name and surname" +
                                                            Environment.NewLine + "State is Invalid");
                    }
                    if(contact.Count == 1)
                    {
                        var contactToBeRemoved = contact.First();
                        contactInformation = contactToBeRemoved;
                        contactsWithSuchName.Remove(contactToBeRemoved);
                        contactsWithSuchName.Remove(contactToBeRemoved);
                        return true;
                    }
                }
            }
            return false;
        }
        public bool GetContactsInformationByNameAndSurname(string name, string surname, out ContactInformation contact)
        {
            contact = null;
            if (nameToTotalInformationConnector.TryGetValue(name, out List<ContactInformation> contactsWithName))
            {
                var foundedContact = contactsWithName.FindAll(x => x.Name == name && x.Surname == surname);
                if(foundedContact.Count> 1)
                {
                    throw new DirectoryStorageException("More than one contact have the same name and surname" +
                                                        Environment.NewLine + "State is Invalid");
                }
                if(foundedContact.Count == 1)
                {
                    contact = foundedContact.First();
                    return true;
                }
            }
            return false;
        }
    }

    public class DirectoryStorageException : Exception
    {
        public DirectoryStorageException()
        {
        }
        public DirectoryStorageException(string message)
            : base(message)
        {
        }

        public DirectoryStorageException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
