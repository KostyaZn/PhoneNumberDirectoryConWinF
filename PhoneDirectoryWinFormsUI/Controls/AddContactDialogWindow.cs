﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using PhoneDirectoryKernel.Controller;

namespace PhoneDirectoryWinFormsUI
{
    public partial class AddContactDialogWindow : Form
    {
        private DataForView dataForView;
        public AddContactDialogWindow()
        {
            InitializeComponent();
        }

        public AddContactDialogWindow(DataForView dataForView)
        {
            this.dataForView = dataForView;
            InitializeComponent();
        }
        private void AddContactButton_Click(object sender, EventArgs e)
        {
            if (NameTextBox.Text == "")
            {
                MessageBox.Show("Name should not be empty");
            }
            else if (SurnameTextBox.Text == "")
            {
                MessageBox.Show("Surname should not be empty");
            }
            else if (PhoneNumberTextBox.Text == "")
            {
                MessageBox.Show("Phone number should not be empty");
            }
            else
            {
                if (dataForView.AddContact(NameTextBox.Text.Trim(), SurnameTextBox.Text.Trim(), PhoneNumberTextBox.Text.Trim()))
                {
                    Close();
                }
            }
        }
        private void CancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
