using System;
using System.Diagnostics;
using System.Windows.Forms;
using PhoneDirectoryKernel.Controller;

namespace PhoneDirectoryWinFormsUI
{
    static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            var controller = new PhoneDirectoryController();
            var dataForView = new DataForView(controller);
            Console.ReadLine();
            Application.Run(new Form1(controller,dataForView));
        }

    }
}
