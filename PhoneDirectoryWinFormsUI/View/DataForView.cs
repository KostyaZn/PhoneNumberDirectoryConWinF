﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Forms;
using PhoneDirectoryKernel.Controller;
using PhoneDirectoryKernel.Model;

namespace PhoneDirectoryWinFormsUI
{
    public class DataForView
    {
        private PhoneDirectoryController controller;

        public  BindingSource BindingSource { get; }

        public DataForView(PhoneDirectoryController controller)
        {
            BindingSource = new BindingSource();
            this.controller = controller;
        }

        public bool AddContact(string name, string surname, string number)
        {
            if (controller.AddContactToDirectory(name, surname, number, out string message, out ContactInformation contactInformation))
            {
                BindingSource.Add(contactInformation);
                return true;
            }
            MessageBox.Show(message);
            return false;
        }

        public bool RemoveContact(string name, string surname)
        {
            if (controller.RemoveContact(name, surname, out string message, out ContactInformation contactInformation))
            {
                BindingSource.Remove(contactInformation);
                return true;
            }
            MessageBox.Show(message);
            return false;
        }

    }
}
