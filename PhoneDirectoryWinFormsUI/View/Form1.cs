﻿using PhoneDirectoryKernel.Model;
using System;
using System.Diagnostics;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Windows.Forms;
using System.ServiceProcess;
using PhoneDirectoryKernel.Controller;
using PhoneNumbersDirectory.View;

namespace PhoneDirectoryWinFormsUI
{
    public partial class Form1 : Form
    {
        private DataForView dataForView;
        private PhoneDirectoryController controller;
        public Form1()
        {
            InitializeComponent();
        }

        public Form1(PhoneDirectoryController controller, DataForView dataForView)
        { 
            InitializeComponent();
            var currentProcess = Process.GetCurrentProcess();
            this.dataForView = dataForView;
            this.controller = controller;
            GridForContactsData.DataSource = dataForView.BindingSource;
            GridForContactsData.ColumnAdded += GridForContactsData_ColumnAdded;
        }

        private void GridForContactsData_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            int count = GridForContactsData.Columns.Count;
            for (int i = 0; i < count; i++)
            {
                GridForContactsData.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            var addDialog = new AddContactDialogWindow(dataForView);
            addDialog.Show();
        }

        private void RemoveButton_Click(object sender, EventArgs e)
        {
            var temp = GridForContactsData.SelectedRows;
            foreach (var selectedRow in temp.OfType<DataGridViewRow>())
            {
                if (selectedRow.DataBoundItem is ContactInformation contactInformation)
                {
                    dataForView.RemoveContact(contactInformation.Name, contactInformation.Surname);
                }
            }
        }
    }
}
