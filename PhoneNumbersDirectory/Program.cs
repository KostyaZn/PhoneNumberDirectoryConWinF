﻿using System;
using System.Diagnostics;
using System.Linq;
using PhoneDirectoryKernel.Controller;
using PhoneNumbersDirectory.View;

namespace PhoneDirectoryConsoleUI
{
    class Program
    {
        static void Main(string[] args)
        {

            string[] database = new[]
            {
                    "NameOne SurnameOne 1111111",
                    "NameTwo SurnameTwo 2222222",
                    "NameTree SurnameTree 3333333",
                    "NameFour SurnameFour 4444444",
                    "NameFive SurnameFive 5555555"
            };
            var consoleInteractor = new ConsolePhoneDirectoryView(database);
            Console.ReadKey();

        }
    }
}
