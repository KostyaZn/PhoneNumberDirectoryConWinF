﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using PhoneDirectoryConsoleUI;
using PhoneDirectoryKernel.Controller;

namespace PhoneNumbersDirectory.View
{
    public class ConsolePhoneDirectoryView
    {
        private PhoneDirectoryController controller;

        private Dictionary<Regex, IConsoleCommandHandler> handlers;

        public ConsolePhoneDirectoryView(string[] dataBase)
        {
            var listOfHandlers = new List<IConsoleCommandHandler>()
            {
                new AddNewContact(),
                new GetContact(),
                new RemoveContact()
            };
            handlers = listOfHandlers
                .ToDictionary(x => new Regex($"^{x.NameOfCommand}"), x => x);
            controller = new PhoneDirectoryController();
            RunConsoleWithDirectoryInteractor(dataBase);
        }

        public ConsolePhoneDirectoryView(PhoneDirectoryController controller)
        {
            var listOfHandlers = new List<IConsoleCommandHandler>()
            {
                new AddNewContact(),
                new GetContact(),
                new RemoveContact()
            };
            handlers = listOfHandlers
                .ToDictionary(x => new Regex($"^{x.NameOfCommand}"), x => x);
            this.controller = controller;
            RunConsoleWithDirectoryInteractor();
        }

        private void RunConsoleWithDirectoryInteractor(string[] dataBase = null)
        {
            if (dataBase?.Length > 0)
            {
                Console.WriteLine("Initialize database");
                var addHandler = new AddNewContact();
                foreach (var contact in dataBase)
                {
                    addHandler.ExecuteCommand(controller,contact);
                }
            }
            Console.WriteLine("To add new contact enter in next format : Add Name Surname Phone Number");
            Console.WriteLine("To delete contact enter in next format : Remove Name Surname");
            Console.WriteLine("To find contact enter in next format : Get Name Surname");
            string input;
            do
            {
                input = Console.ReadLine();
                try
                {
                    foreach (var handler in handlers)

                    {
                        if (handler.Key.IsMatch(input))
                        {

                            var processedInput = handler.Key.Replace(input, "");
                            handler.Value.ExecuteCommand(controller, processedInput);
                        }
                    }
                }
                catch (ArgumentException e)
                {
                    Console.WriteLine(e.Message);
                }
            } while (input != "End" && input != null);
        }

    }
}
