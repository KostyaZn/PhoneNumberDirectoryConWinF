﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using PhoneDirectoryKernel.Controller;
using PhoneDirectoryKernel.Model;

namespace PhoneNumbersDirectory.View
{
    interface IConsoleCommandHandler
    {
        string NameOfCommand { get; }
        void ExecuteCommand(PhoneDirectoryController manager, string userInput);
    }
    class AddNewContact : IConsoleCommandHandler
    {

        private  readonly Regex inputToAddSplitPattern = new Regex(@"[^a-zA-Zа-яА-Я0-9]");
        public string NameOfCommand { get; } = "Add";
        public void ExecuteCommand(PhoneDirectoryController manager, string userInput)
        {
            var contactInformation = inputToAddSplitPattern.Split(userInput)
                .Where(x => x != "").ToArray();
            if (contactInformation.Length != 3)
            {
                Console.WriteLine("Input is incorrect");
            }
            manager.AddContactToDirectory(contactInformation[0].Trim(), contactInformation[1].Trim(), contactInformation[2].Trim(), out string message, out _);
            Console.WriteLine(message);
        }
    }

    class RemoveContact : IConsoleCommandHandler
    {
        private Regex inputGetOrRemoveSplitPattern = new Regex(@"[^a-zA-Zа-яА-Я]");
        public string NameOfCommand { get; } = "Remove";
        public void ExecuteCommand(PhoneDirectoryController manager, string userInput)
        {
            var contactInformation = inputGetOrRemoveSplitPattern.Split(userInput)
                .Where(x => x != "").ToArray();
            if (contactInformation.Length != 2)
            {
                Console.WriteLine("Input is incorrect");
            }
            else
            {
                manager.RemoveContact(contactInformation[0].Trim(), contactInformation[1].Trim(), out string message, out _);
                Console.WriteLine(message);
            }
        }
    }

    class GetContact : IConsoleCommandHandler
    {
        private Regex inputGetOrRemoveSplitPattern = new Regex(@"[^a-zA-Zа-яА-Я]");
        public string NameOfCommand { get; } = "Get";
        public void ExecuteCommand(PhoneDirectoryController manager, string userInput)
        {
            var contactInformation = inputGetOrRemoveSplitPattern.Split(userInput)
                .Where(x => x != "").ToArray();
            if (contactInformation.Length != 2)
            {
                Console.WriteLine("Input is incorrect");
            }
            else if (manager.GetContact(contactInformation[0].Trim(), contactInformation[1].Trim(),  out string message, out ContactInformation contact))
            {
                Console.WriteLine($"{contact.Name} {contact.Surname} {contact.PhoneNumber}");
            }
            else
            {
                Console.WriteLine(message);
            }
        }
    }
}
